package com.creator.simplify;

// 建造者模式 测试类 (简化版)
// 整合 指挥类 和 抽象构建者
public class Main {
    public static void main(String[] args) {

        Bike bike = new OfoBuilder().construct();

        System.out.println("Ofo自行车: ");
        System.out.println("bike.getFrame() = " + bike.getFrame());
        System.out.println("bike.getSeat() = " + bike.getSeat());

    }
}
