package com.creator.simplify;

// 构建器
public abstract class Builder {

    // 其他包不能访问
    protected Bike bike = new Bike();
    // 构建 Bike车架
    public abstract void buildFrame();
    // 构建 Bike车座
    public abstract void buildSeat();
    // 构建 自行车方法
    public abstract Bike createBike();

    // 组装自行车
    public Bike construct(){
        this.buildFrame();
        this.buildSeat();
        return this.createBike();
    }

}
