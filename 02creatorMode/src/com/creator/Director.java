package com.creator;

// 指挥者
public class Director {

    public Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    // 组装自行车
    public Bike construct(){
        builder.buildFrame();
        builder.buildSeat();
        return builder.createBike();
    }
}
