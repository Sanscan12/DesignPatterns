package com.creator;

// 建造者模式 测试类
public class Main {
    public static void main(String[] args) {

        Director director = new Director(new MobileBuilder());

        Bike bike = director.construct();

        System.out.println("bike.getFrame() = " + bike.getFrame());
        System.out.println("bike.getSeat() = " + bike.getSeat());

    }
}
