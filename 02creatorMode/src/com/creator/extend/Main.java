package com.creator.extend;

// 构建者 扩展应用 测试类
public class Main {
    public static void main(String[] args) {
        Computer builder = new Computer.Builder()
                .cpu("intel9400")
                .memory("金士顿内存条16G")
                .hardDisk("西数硬盘500G")
                .mainboard("华硕主板")
                .builder();
        System.out.println(builder);
    }
}
