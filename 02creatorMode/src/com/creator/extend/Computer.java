package com.creator.extend;

// 计算机
public class Computer {

    private String cpu;
    private String hardDisk;
    private String mainboard;
    private String memory;

    private Computer(Builder builder) {
        this.cpu = builder.cpu;
        this.hardDisk = builder.hardDisk;
        this.mainboard = builder.mainboard;
        this.memory = builder.memory;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "cpu='" + cpu + '\'' +
                ", hardDisk='" + hardDisk + '\'' +
                ", mainboard='" + mainboard + '\'' +
                ", memory='" + memory + '\'' +
                '}';
    }

    public static final class Builder {
        private String cpu;
        private String hardDisk;
        private String mainboard;
        private String memory;

        public Computer builder() {
            return new Computer(this);
        }

        public Builder cpu(String cpu) {
            this.cpu = cpu;
            return this;
        }

        public Builder hardDisk(String hardDisk) {
            this.hardDisk = hardDisk;
            return this;
        }

        public Builder mainboard(String mainboard) {
            this.mainboard = mainboard;
            return this;
        }

        public Builder memory(String memory) {
            this.memory = memory;
            return this;
        }
    }

}
