package com.factory.before;

// 咖啡店类
public class CoffeeStore {

    public Coffee orderCoffee(String type) {
        Coffee coffee = null;
        switch (type) {
            case "美式咖啡":
                coffee = new AmericanCoffee();
                break;
            case "拿铁咖啡":
                coffee = new LatteCoffee();
                break;
            default:
                System.out.println("对不起没有您选择的咖啡!");
        }
        // 添加配料
        coffee.addsugar();
        coffee.addMilk();

        return coffee;
    }

}
