package com.factory.before;

/* 测试类
    手写模拟工厂类
 */
public class Main {
    public static void main(String[] args) {
        CoffeeStore store = new CoffeeStore();
        // 点咖啡
        Coffee coffee = store.orderCoffee("美式咖啡");
        System.out.println(coffee.getName());
    }
}
