package com.factory.method;



// 咖啡店
public class CoffeeStore {

    private CoffeeFactory factory;

    public void setFactory(CoffeeFactory factory) {
        this.factory = factory;
    }

    // 点咖啡
    public Coffee orderCoffee() {

        Coffee coffee = factory.createCoffee();

        coffee.addSugar();
        coffee.addMilk();
        return coffee;
    }

}
