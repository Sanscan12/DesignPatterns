package com.factory.method;

// 咖啡工厂接口
public interface CoffeeFactory {

    // 创建咖啡方法
    Coffee createCoffee();

}
