package com.factory.method;

// 测试 方法工厂
public class Main {
    public static void main(String[] args) {
        CoffeeStore store = new CoffeeStore();
//        AmericanCoffeeFactory factory = new AmericanCoffeeFactory();
        LatteCoffeeFactory factory = new LatteCoffeeFactory ();
        store.setFactory(factory);
        Coffee coffee = store.orderCoffee();
        System.out.println(coffee.getName());
    }
}
