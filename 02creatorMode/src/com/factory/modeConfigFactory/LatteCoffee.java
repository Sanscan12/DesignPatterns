package com.factory.modeConfigFactory;

// 拿铁咖啡
public class LatteCoffee extends Coffee {
    @Override
    String getName() {
        return "拿铁咖啡";
    }
}
