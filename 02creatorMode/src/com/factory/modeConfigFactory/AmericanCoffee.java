package com.factory.modeConfigFactory;

// 美式咖啡
public class AmericanCoffee extends Coffee {
    @Override
    String getName() {
        return "美式咖啡";
    }
}
