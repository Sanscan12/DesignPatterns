package com.factory.modeConfigFactory;

// 模式扩展
public class Main {
    public static void main(String[] args) {
        // 提取美式咖啡
        Coffee american = CoffeeFactory.createCoffee("american");
        System.out.println("american = " + american.getName());

        System.out.println("=========");
        // 提取拿铁咖啡
        Coffee american2 = CoffeeFactory.createCoffee("latte");
        System.out.println("american2 = " + american2.getName());
    }
}
