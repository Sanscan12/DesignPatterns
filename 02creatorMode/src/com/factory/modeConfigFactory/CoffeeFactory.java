package com.factory.modeConfigFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

// 咖啡工厂
public class CoffeeFactory {

    // 加载配置文件 , 从配置文件进行加载对象
    // 1. 定义容器对象存储咖啡
    private static HashMap<String, Coffee> map = new HashMap<>();

    // 2. 加载配置文件
    static {
        Properties properties = new Properties();
        // 在类加载器的根路径进行加载配置文件
        try(InputStream is = CoffeeFactory.class.getClassLoader().getResourceAsStream("bean.properties")) {
            properties.load(is);
            // 创建对象进行存储
            Set<Object> keys = properties.keySet();
            for (Object key : keys) {
                // 获取指定key的值 (全限定类名)
                String className = properties.getProperty((String) key);
                // 通过反射创建对象
                Class<?> aClass = Class.forName(className);
                Coffee coffee = (Coffee) aClass.newInstance();
                // 存储map
                map.put((String)key , coffee);
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static Coffee createCoffee(String name) {
        return map.get(name);
    }
}
