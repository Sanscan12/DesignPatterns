package com.factory.simple;

// 咖啡店类
public class CoffeeStore {

    public Coffee orderCoffee(String type) {
        CoffeeFactory coffeeFactory = new CoffeeFactory();
        Coffee coffer = coffeeFactory.createCoffee(type);
        // 添加配料
        coffer.addSugar();
        coffer.addMilk();

        return coffer;
    }

}
