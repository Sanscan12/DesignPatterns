package com.factory.simple;

// 咖啡工厂
public class CoffeeFactory {

    // 如果应用的是 静态工程 添加添加静态修饰接口
    public Coffee createCoffee(String type) {
        Coffee coffee = null;
        switch (type) {
            case "美式咖啡":
                coffee = new AmericanCoffee();
                break;
            case "拿铁咖啡":
                coffee = new LatteCoffee();
                break;
            default:
                System.out.println("对不起没有您选择的咖啡!");
        }
        return coffee;
    }

}
