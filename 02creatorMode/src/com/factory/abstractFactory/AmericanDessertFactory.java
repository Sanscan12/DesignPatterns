package com.factory.abstractFactory;

// 美国甜品厂
//  生产 : 抹茶慕斯 , 美式咖啡
public class AmericanDessertFactory implements DessertFactory{
    @Override
    public Coffee createCoffee() {
        return new AmericanCoffee();
    }

    @Override
    public Dessert createDessert() {
        return new MatchaMousse();
    }
}
