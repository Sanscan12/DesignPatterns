package com.factory.abstractFactory;

// 提拉米苏
public class Tiramisu extends Dessert{
    @Override
    String getName() {
        return "提拉米苏";
    }
}
