package com.factory.abstractFactory;

// 甜品工厂
public interface DessertFactory {

    // 咖啡
    Coffee createCoffee();

    // 甜心
    Dessert createDessert();

}
