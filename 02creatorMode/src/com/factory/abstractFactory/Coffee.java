package com.factory.abstractFactory;

// 咖啡
public abstract class Coffee {

    abstract String getName();

    public void addMilk(){
        System.out.println("加奶");
    }

    public void addSugar(){
        System.out.println("加糖");
    }

}
