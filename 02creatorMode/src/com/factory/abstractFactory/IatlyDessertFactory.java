package com.factory.abstractFactory;

// 意大利甜品厂
//  生产 : 提拉米苏 , 拿铁咖啡
public class IatlyDessertFactory implements DessertFactory{
    @Override
    public Coffee createCoffee() {
        return new LatteCoffee();
    }

    @Override
    public Dessert createDessert() {
        return new Tiramisu();
    }
}
