package com.factory.abstractFactory;

// 抹茶慕斯
public class MatchaMousse extends Dessert{
    @Override
    String getName() {
        return "抹茶慕斯";
    }
}
