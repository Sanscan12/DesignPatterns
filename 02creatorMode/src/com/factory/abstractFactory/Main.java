package com.factory.abstractFactory;

// 测试 抽象工厂
public class Main {
    public static void main(String[] args) {
        // 意大利风味甜品工厂对象
        IatlyDessertFactory iatlyDessertFactory = new IatlyDessertFactory();
        // 该工厂生产 提拉米苏 , 拿铁咖啡
        Coffee coffee = iatlyDessertFactory.createCoffee();
        Dessert dessert = iatlyDessertFactory.createDessert();
        System.out.println("意大利风味甜品工厂 产生以下食品 : ");
        System.out.println("coffee = " + coffee.getName());
        System.out.println("dessert = " + dessert.getName());

        System.out.println("================");
        // 美式风味甜品工厂对象
        AmericanDessertFactory americanDessertFactory = new AmericanDessertFactory();
        // 该工厂生产 抹茶慕斯 , 美式咖啡
        Coffee coffee2 = americanDessertFactory.createCoffee();
        Dessert dessert2 = americanDessertFactory.createDessert();
        System.out.println("美式风味甜品工厂 产生以下食品 : ");
        System.out.println("coffee2 = " + coffee2.getName());
        System.out.println("dessert2 = " + dessert2.getName());

    }
}
