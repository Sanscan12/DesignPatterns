package com.singleton;

import java.io.Serializable;

// 单例模式 饿汉式
public class HungrySingleton implements Serializable {
    // 构造方法私有化
    private HungrySingleton(){}

    private static class Singleton {
        private static final HungrySingleton INSTANCE = new HungrySingleton();
    }

    public static HungrySingleton getInstance() {
        return Singleton.INSTANCE;
    }

    // 在Singleton类中添加 readResolve()方法，在反序列化时被反射调用，如果定义了这个方法，
    // 就返回这个方法的值，如果没有定义，则返回新new出来的对象
    private Object  readResolve(){
        return Singleton.INSTANCE;
    }
}
