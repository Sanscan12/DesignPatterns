package com.singleton.qaDemo;

import com.singleton.LazySingleton;

import java.lang.reflect.Constructor;

// 反射 破坏单例模式
public class ReflectionDamage {
    public static void main(String[] args) throws Exception {

        Class aClass = LazySingleton.class;
        // 获取对象构造方法
        Constructor cons = aClass.getDeclaredConstructor();
        // 取消访问检查 (越过权限修饰符
        cons.setAccessible(true);

        LazySingleton singleton = (LazySingleton) cons.newInstance();
        LazySingleton singleton2 = (LazySingleton) cons.newInstance();

        System.out.println("singleton = " + singleton);
        System.out.println("singleton2 = " + singleton2);

    }
}

   