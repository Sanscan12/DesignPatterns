package com.singleton;

/** 单例模式 测试类
 *
 */
public class Main {
    public static void main(String[] args) {

        // 饿汉式测试
        HungrySingleton hungrySingleton = HungrySingleton.getInstance();
        HungrySingleton hungrySingleton2 = HungrySingleton.getInstance();
        System.out.println(hungrySingleton == hungrySingleton2);
        System.out.println("===========");

        // 懒汉式测试
        LazySingleton lazySingleton = LazySingleton.getInstance();
        LazySingleton lazySingleton2 = LazySingleton.getInstance();
        System.out.println(lazySingleton == lazySingleton2);

    }
}
