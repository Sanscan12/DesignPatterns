package com.singleton;

import java.io.Serializable;

// 单例模式 懒汉式 (双重检查锁机制 (volatile关键字&同步锁
public class LazySingleton implements Serializable {
    // 静态 保证 所有线程中都是同步
    private static volatile LazySingleton instance = null;

    // 控制对象创建
    private static Boolean flag = false;

    // 构造器 私有化
    private LazySingleton() {
        synchronized (LazySingleton.class){
            if (flag){
                throw new RuntimeException("不能创建多个对象");
            }
            flag = true;
        }
    }

    // 同步锁 防止多线程共同创建可能产生一个以上的实例
    public static LazySingleton getInstance() {
    //第一次判断，如果instance不为null，不进入抢锁阶段，直接返回实际
        if (instance == null) {
            synchronized (LazySingleton.class) {
                //抢到锁之后再次判断是否为空
                if (instance == null) {
                    instance = new LazySingleton();
                }
            }
        }
        return instance;
    }

}
