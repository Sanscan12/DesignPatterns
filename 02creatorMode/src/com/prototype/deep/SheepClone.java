package com.prototype.deep;

import java.io.Serializable;

// 克隆绵羊
public class SheepClone implements Cloneable, Serializable {

    private Sheep sheep;

    public Sheep getSheep() {
        return sheep;
    }

    public void setSheep(Sheep sheep) {
        this.sheep = sheep;
    }

    public void show() {
        System.out.println(sheep.getName()+" 克隆绵羊!");
    }

    // 克隆对象
    @Override
    protected SheepClone clone() throws CloneNotSupportedException {
        return (SheepClone) super.clone();
    }
}
