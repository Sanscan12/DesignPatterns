package com.prototype.deep;

import java.io.Serializable;

// 绵羊类
public class Sheep implements Cloneable, Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
