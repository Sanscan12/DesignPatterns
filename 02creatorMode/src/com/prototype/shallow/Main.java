package com.prototype.shallow;

// 原型工厂 测试类(克隆应用)
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {

        // 构造方法只执行了一次 , 克隆不会执行构造方法
        Sheep sheep = new Sheep();
        sheep.setName("小绵羊");
        System.out.println("sheep : " + sheep.getName());

        Sheep sheep2 = sheep.clone();
        System.out.println("sheep2(1) : " + sheep2.getName());
        sheep2.setName("多莉");
        System.out.println("sheep2(2) : " + sheep2.getName());

        System.out.println("sheep : " + sheep);
        System.out.println("sheep2 : " + sheep2);
        System.out.println("sheep == sheep2?"+(sheep == sheep2));

    }
}
