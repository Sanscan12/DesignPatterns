package com.prototype.shallow;

// 羊
public class Sheep implements Cloneable{

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sheep() {
        System.out.println("构造方法创建 羊!!");
    }

    // Object
    @Override
    protected Sheep clone() throws CloneNotSupportedException {
        System.out.println("进行克隆对象!!!");
        return (Sheep) super.clone();
    }
}
