package com.chainOfResponsibility;

// 责任链 请假案例
public class Main {
    public static void main(String[] args) {
        // 请假对象
        LeaveRequest leave = new LeaveRequest("小明" , 6 ,"身体不适");

        // 领导对象
        GroupLeader groupLeader = new GroupLeader();
        Manager manager = new Manager();
        GeneralManager generalManager = new GeneralManager();

        // 设置处理者链
        groupLeader.nextHandler(manager);
        manager.nextHandler(generalManager);

        // 小明提交请假
        groupLeader.submit(leave);

    }
}
