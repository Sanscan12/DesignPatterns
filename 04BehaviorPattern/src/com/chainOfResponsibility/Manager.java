package com.chainOfResponsibility;

// 经理
public class Manager extends Handler{

    public Manager() {
        super(0, Handler.NUM_THREE);
    }

    @Override
    protected void handleleave(LeaveRequest leaveRequest) {
        System.out.println("经理  审批同意");
        System.out.println(leaveRequest.getName() + "请假" + leaveRequest.getNum() + "天,"+leaveRequest.getContent());
    }
}
