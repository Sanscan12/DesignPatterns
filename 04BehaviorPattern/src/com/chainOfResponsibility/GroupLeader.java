package com.chainOfResponsibility;

// 组长
public class GroupLeader extends Handler{

    public GroupLeader() {
        super(Handler.NUM_ONE, Handler.NUM_ONE);
    }

    @Override
    protected void handleleave(LeaveRequest leaveRequest) {
        System.out.println("组长  审批同意");
        System.out.println(leaveRequest.getName() + "请假" + leaveRequest.getNum() + "天,"+leaveRequest.getContent());
    }
}
