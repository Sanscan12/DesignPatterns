package com.chainOfResponsibility;

// 总经理
public class GeneralManager extends Handler{

    public GeneralManager() {
        super(Handler.NUM_THREE, Handler.NUM_SEVEN);
    }

    @Override
    protected void handleleave(LeaveRequest leaveRequest) {
        System.out.println("总经理  审批同意");
        System.out.println(leaveRequest.getName() + "请假" + leaveRequest.getNum() + "天,"+leaveRequest.getContent());
    }
}
