package com.chainOfResponsibility;

// 抽象处理者
public abstract class Handler {

    // 请假天数的临界点
    protected static final int NUM_ONE = 1;
    protected static final int NUM_THREE = 3;
    protected static final int NUM_SEVEN = 7;

    // 处理请假天数区间
    private int numStart;
    private int numEnd;

    // 上级
    private Handler nextHandler;

    public Handler(int numStart, int numEnd) {
        this.numStart = numStart;
        this.numEnd = numEnd;
    }

    // 设置上级领导独享
    public void nextHandler(Handler handler) {
        this.nextHandler = handler;
    }

    // 处理清请假条方法
    protected abstract void handleleave(LeaveRequest leaveRequest);

    // 提交请假条
    public void submit(LeaveRequest leaveRequest) {
        this.handleleave(leaveRequest);
        if (this.nextHandler != null && leaveRequest.getNum() > this.numEnd) {
            this.nextHandler.submit(leaveRequest);
        }else{
            System.out.println("流程结束!");
        }
    }

}
