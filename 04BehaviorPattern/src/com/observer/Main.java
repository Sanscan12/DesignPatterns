package com.observer;

// 观察者模式
public class Main {
    public static void main(String[] args) {

        // 创建公众号
        SubscriptionSubject subject = new SubscriptionSubject();

        // 订阅公众号 (添加观察者)
        subject.attach(new WeixinUser("孙悟空"));
        subject.attach(new WeixinUser("猪八戒"));
        subject.attach(new WeixinUser("沙悟净"));

        // 公众号通知
        subject.notify("明天下大雨!");

    }
}
