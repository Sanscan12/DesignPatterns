package com.observer;

// 微信用户
public class WeixinUser implements Observer{

    String name;

    public WeixinUser(String name) {
        this.name = name;
    }

    @Override
    public void update(String message) {
        System.out.println(name + " - " + message);
    }
}
