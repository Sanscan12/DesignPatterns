package com.observer;

// 抽象观察者接口
public interface Observer {
    // 主题推送内容
    void update(String message);

}
