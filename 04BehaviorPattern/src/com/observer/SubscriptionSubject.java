package com.observer;

import java.util.ArrayList;
import java.util.List;

// 具体主题类
public class SubscriptionSubject implements Subject{
    // 存储 订阅者/观察者 对象
    List<Observer> weixinUserList = new ArrayList<>();

    @Override
    public void attach(Observer observer) {
        weixinUserList.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        weixinUserList.remove(observer);
    }

    @Override
    public void notify(String message) {
        for (Observer observer : weixinUserList) {
            observer.update(message);
        }
    }
}
