package com.visitor;

// 宠物 猫 具体元素角色类
public class Cat implements Animal{
    @Override
    public void accept(Person person) {
        person.feed(this);
        System.out.println("喂猫ing...");
    }
}
