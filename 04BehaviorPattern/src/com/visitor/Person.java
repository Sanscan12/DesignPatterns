package com.visitor;

// 访问者接口 抽象访问者
public interface Person {
    // 喂猫
    void feed(Cat cat);
    // 喂狗
    void feed(Dog dog);

}
