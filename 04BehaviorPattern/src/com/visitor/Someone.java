package com.visitor;

// 外人 具体访问者
public class Someone implements Person{
    @Override
    public void feed(Cat cat) {
        System.out.println("外人 喂操作(猫)");
    }

    @Override
    public void feed(Dog dog) {
        System.out.println("外人 喂操作(狗)");
    }
}
