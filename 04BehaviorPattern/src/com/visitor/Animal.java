package com.visitor;

// 动物接口 抽象元素角色
public interface Animal {
    // 接收 访问者
    void accept(Person person);
}
