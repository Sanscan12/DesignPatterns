package com.visitor;

import java.util.ArrayList;
import java.util.List;

// 对象结构类
public class Home {
    // 存储宠物对象集合
    private List<Animal> nodeList = new ArrayList<>();

    // 访问所有宠物
    public void action(Person person) {
        for (Animal animal : nodeList) {
            animal.accept(person);
        }
    }

    // 添加宠物
    public void addAnimal(Animal animal) {
        nodeList.add(animal);
    }

}
