package com.visitor;

// 自己 具体访问者
public class Owner implements Person{
    @Override
    public void feed(Cat cat) {
        System.out.println("本人 喂操作(猫)");
    }

    @Override
    public void feed(Dog dog) {
        System.out.println("本人 喂操作(狗)");
    }
}
