package com.visitor;

// 访问者模式
public class Main {
    public static void main(String[] args) {

        // 创建Home对象
        Home home = new Home();
        // 添加宠物对象
        home.addAnimal(new Dog());
        home.addAnimal(new Cat());
        // 创建访问者
        Owner owner = new Owner();
        Someone someone = new Someone();

        // 访问 喂食操作
        home.action(owner);

    }
}
