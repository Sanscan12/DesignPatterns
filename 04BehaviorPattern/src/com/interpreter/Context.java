package com.interpreter;

import java.util.HashMap;
import java.util.Map;

// 环境角色
public class Context {
    // 存储变量值
    Map<Variable, Integer> map = new HashMap<>();

    // 添加变量
    public void assign(Variable var, Integer value) {
        map.put(var, value);
    }

    // 获取对应变量的值
    public int getValue(Variable variable) {
        return map.get(variable);
    }

}
