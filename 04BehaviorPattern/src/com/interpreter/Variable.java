package com.interpreter;

// 封装变量类
public class Variable extends AbstractExpression {

    // 存储变量名
    private String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public int  interpret(Context context) {
        return context.getValue(this);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
