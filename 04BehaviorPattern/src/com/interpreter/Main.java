package com.interpreter;

// 解释器
public class Main {
    public static void main(String[] args) {
        // 创建环境对象
        Context context = new Context();

        // 创建变量对象
        Variable a = new Variable("a");
        Variable b = new Variable("b");
        Variable c = new Variable("c");
        Variable d = new Variable("d");
        Variable e = new Variable("e");

        // 将变量存储到抽象语法树
        context.assign(a, 1);
        context.assign(b, 2);
        context.assign(c, 3);
        context.assign(d, 4);
        context.assign(e, 5);

        // 获取抽象语法树
        AbstractExpression expression = new Minus(a, new Plus(new Minus(b, c), d));

        // 计算
        int interpret = expression.interpret(context);
        System.out.print(expression);
        System.out.println(" = " + interpret);
    }
}
