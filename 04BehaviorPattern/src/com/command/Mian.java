package com.command;

// 命令模式
public class Mian {
    public static void main(String[] args) {
        // new订单1
        Order order1 = new Order();
        order1.setDiningTable(1);
        order1.setFoodDic("西红柿", 1);
        order1.setFoodDic("可乐", 2);

        // new订单2
        Order order2 = new Order();
        order2.setDiningTable(2);
        order2.setFoodDic("鸡肉卷", 1);
        order2.setFoodDic("可乐", 2);

        // new厨师
        SeniorChef receiver = new SeniorChef();
        // 创建 命令对象
        OrderCommand cmd1 = new OrderCommand(receiver, order1);
        OrderCommand cmd2 = new OrderCommand(receiver, order2);

        // 创建调用者
        Waitor invoke = new Waitor();
        // 设置两条命令
        invoke.setCommands(cmd1);
        invoke.setCommands(cmd2);

        // 服务员发起命令
        invoke.orderUp();
    }
}
