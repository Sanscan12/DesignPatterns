package com.command;

// 厨师类
public class SeniorChef {

    // 制作食物
    public void makeFood(int num , String foodName) {
        System.out.println(foodName+" x"+num+" 制作中...");
    }
    
}
