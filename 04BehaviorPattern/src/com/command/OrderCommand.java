package com.command;

import java.util.Map;

// 具体命令类
public class OrderCommand implements Command{
    // 持有接收者对象 (厨师)
    SeniorChef receiver;
    // 订单
    Order order;

    public OrderCommand(SeniorChef receiver, Order order) {
        this.receiver = receiver;
        this.order = order;
    }

    @Override
    public void execute() {
        System.out.println("执行订单命令");
        System.out.println(this.order.getDiningTable()+" 桌的订单 :");
        // 取出订单
        Map<String, Integer> foodDir = order.getFoodDic();
        // 遍历map集合
        foodDir.forEach((k,v) -> {
            receiver.makeFood(v,k);
        });
        System.out.println(this.order.getDiningTable()+" 桌的订单完成!");
    }
}
