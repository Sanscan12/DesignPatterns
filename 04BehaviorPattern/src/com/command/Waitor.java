package com.command;

import java.util.ArrayList;
import java.util.List;

// 服务员 请求者角色
public class Waitor {
    // 持有多个命令对象
    private List<Command> commands = new ArrayList<>();

    // 添加命令
    public void setCommands(Command cmd) {
        this.commands.add(cmd);
    }

    // 发起命令 , 发给厨师
    public void orderUp() {
        System.out.println("大厨订单来啦");
        for (Command command : this.commands) {
            if (command != null)
                command.execute();
        }
    }

}
