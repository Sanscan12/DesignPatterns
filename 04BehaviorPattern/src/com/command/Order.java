package com.command;

import java.util.HashMap;
import java.util.Map;

// 订单类
public class Order {
    // 餐桌号
    private int diningTable;
    // 下单份数
    private Map<String, Integer> foodDic = new HashMap<>();

    public int getDiningTable() {
        return this.diningTable;
    }

    public void setDiningTable(int diningTable) {
        this.diningTable = diningTable;
    }

    public Map<String, Integer> getFoodDic() {
        return foodDic;
    }

    public void setFoodDic(String name, int num) {
        foodDic.put(name, num);
    }
}
