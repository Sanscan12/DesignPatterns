package com.state;

// 打开状态
public class OpenningState extends LiftState{

    @Override
    public void open() {
        System.out.println("电梯启动...");
    }

    @Override
    public void close() {
        // 修改状态
        super.context.setLiftState(Context.CLOSINGSTATE);
        // 调用当前状态中的context中的close方法
        super.context.close();
    }

    @Override
    public void stop() {
        // null
    }

    @Override
    public void run() {
        // null
    }
}
