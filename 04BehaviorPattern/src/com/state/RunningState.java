package com.state;

// 运行状态
public class RunningState extends LiftState{

    @Override
    public void open() {
        // null
    }

    @Override
    public void close() {
        // null
    }

    @Override
    public void stop() {
        super.context.setLiftState(Context.STOPPINGSTATE);
        super.context.stop();
    }

    @Override
    public void run() {
        System.out.println("电梯正在运行...");
    }
}
