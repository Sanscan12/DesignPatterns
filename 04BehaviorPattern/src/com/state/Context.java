package com.state;

// 环境
public class Context {

    // 状态对象
    protected final static OpenningState OPENNINGSTATE = new OpenningState();
    protected final static ClosingState CLOSINGSTATE = new ClosingState();
    protected final static RunningState RUNNINGSTATE = new RunningState();
    protected final static StoppingState STOPPINGSTATE = new StoppingState();

    // 电梯状态
    LiftState liftState;

    public LiftState getLiftState() {
        return liftState;
    }
    // 设置当前状态
    public void setLiftState(LiftState liftState) {
        this.liftState = liftState;
        // 设置 当前状态对象中的Context对象
        this.liftState.setContext(this);
    }

    public void open() {
        this.liftState.open();
    }

    public void close() {
        this.liftState.close();
    }

    public void stop() {
        this.liftState.stop();
    }

    public void run() {
        this.liftState.run();
    }

}
