package com.state;

public class Main {
    public static void main(String[] args) {
        // 创建环境角色
        Context context = new Context();
        // 设置当前电梯
        context.setLiftState(new ClosingState());

        context.open();
        context.run();
        context.close();
        context.stop();

    }
}
