package com.state;

// 停止状态
public class StoppingState extends LiftState{
    @Override
    public void open() {
        super.context.setLiftState(Context.OPENNINGSTATE);
        super.context.open();
    }

    @Override
    public void close() {
        super.context.setLiftState(Context.CLOSINGSTATE);
        super.context.close();
    }

    @Override
    public void stop() {
        System.out.println("电梯停止...");
    }

    @Override
    public void run() {
        super.context.setLiftState(Context.RUNNINGSTATE);
        super.context.run();
    }
}
