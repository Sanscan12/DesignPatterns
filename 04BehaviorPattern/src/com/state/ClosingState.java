package com.state;

// 关闭状态
public class ClosingState extends LiftState{
    @Override
    public void open() {
        super.context.setLiftState(Context.OPENNINGSTATE);
        super.context.open();
    }

    @Override
    public void close() {
        System.out.println("电梯门关闭...");
    }

    @Override
    public void stop() {
        super.context.setLiftState(Context.OPENNINGSTATE);
        super.context.stop();
    }

    @Override
    public void run() {
        super.context.setLiftState(Context.RUNNINGSTATE);
        super.context.run();
    }
}
