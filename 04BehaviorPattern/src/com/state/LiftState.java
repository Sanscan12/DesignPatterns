package com.state;

// 抽象状态类
public abstract class LiftState {

    // 环境角色
    Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    // 电梯状态方法
    public abstract void open();
    public abstract void close();
    public abstract void stop();
    public abstract void run();

}
