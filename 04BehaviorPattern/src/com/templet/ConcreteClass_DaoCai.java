package com.templet;

// 抄包菜 具体实现类
public class ConcreteClass_DaoCai extends AbstractClass{
    @Override
    public void pourVegetable() {
        System.out.println("下锅包菜");
    }

    @Override
    public void pourSauce() {
        System.out.println("辣椒");
    }
}
