package com.templet;

// 抽象类
public abstract class AbstractClass {

    // 模板方法定义
    // 流程: 倒油 -> 热油 -> 倒菜 -> 倒调料 -> 炒菜
    public void cookProcess() {
        pourOil();
        heatOil();
        pourVegetable();
        pourSauce();
        fry();
    }

    public void pourOil() {
        System.out.println("倒油");
    }

    public void heatOil() {
        System.out.println("热油");
    }

    // 倒菜 (不确定到什么菜进去)
    public abstract void pourVegetable();

    // 倒调料 (不确定)
    public abstract void pourSauce();

    public void fry() {
        System.out.println("炒菜至熟");
    }

}
