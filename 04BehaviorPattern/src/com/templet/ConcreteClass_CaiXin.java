package com.templet;

// 炒菜心 具体实现类
public class ConcreteClass_CaiXin extends AbstractClass{
    @Override
    public void pourVegetable() {
        System.out.println("下锅菜心");
    }

    @Override
    public void pourSauce() {
        System.out.println("盐");
    }
}
