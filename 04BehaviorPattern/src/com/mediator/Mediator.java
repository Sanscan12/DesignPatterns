package com.mediator;

// 抽象中介者类
public interface Mediator {

    // 交互通信方法
    void constact(String message , Person oneself , Person otherSide);
    // 添加同时对象
    void addPerson(Person person);

}
