package com.mediator;

// 中介者模式
public class Main {
    public static void main(String[] args) {

        // 创建中介者对象
        MediatorStructure mediator = new MediatorStructure();
        // 创建租房者对象
        Tenant tenant1 = new Tenant("ls1" , mediator);
        Tenant tenant2 = new Tenant("ls2" , mediator);
        Tenant tenant3 = new Tenant("ls3" , mediator);
        // 创建房主对象
        HouseOwner houseOwner1 = new HouseOwner("zs1",mediator);
        HouseOwner houseOwner2 = new HouseOwner("zs2",mediator);
        HouseOwner houseOwner3 = new HouseOwner("zs3",mediator);

        // 中介者自动具体 房主和租房者
        mediator.addPerson(tenant1);
        mediator.addPerson(tenant2);
        mediator.addPerson(houseOwner1);
        mediator.addPerson(houseOwner2);

        // 正常
        tenant1.constact("我要租房!" , houseOwner2);
        houseOwner2.constact("我这里有!!" , tenant1);


        // 无中介
//        tenant1.constact("我要租房!" , houseOwner3);
//        houseOwner1.constact("我这里有!!" , tenant1);

    }
}
