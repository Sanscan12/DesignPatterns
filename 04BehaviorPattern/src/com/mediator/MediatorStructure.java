package com.mediator;

import java.util.ArrayList;
import java.util.List;

// 具体中介者
public class MediatorStructure implements Mediator{

    // 交互对象
    protected List<Person> personList = new ArrayList<>();

    @Override
    public void addPerson(Person person) {
        personList.add(person);
    }

    // 通信交互方法
    @Override
    public void constact(String message, Person person , Person otherSide) {
        // 判断双方必须存在
        if (!(personList.contains(person) && personList.contains(otherSide))) {
            System.out.println(otherSide.getName()+"未联系中介!");
            return;
        }
        otherSide.getMessage(message , person);
    }

}
