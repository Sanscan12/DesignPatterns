package com.mediator;

// 租房者 具体同事类
public class Tenant extends Person{

    public Tenant(String name, Mediator mediator) {
        super(name, mediator);
    }

    // 通信交互方法
    public void constact(String message , Person otherSide) {
        mediator.constact(message , this , otherSide);
    }

    // 获取对方信息
    @Deprecated
    public void getMessage(String message, Person otherSide) {
        System.out.println("房主("+otherSide.getName()+") : " + message);
    }

}
