package com.tactics;

// 策略接口
public interface Strategy {
    void show();
}
