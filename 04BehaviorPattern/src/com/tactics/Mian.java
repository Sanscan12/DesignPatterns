package com.tactics;

// 策略模式
public class Mian {
    public static void main(String[] args) {
        // 节日A
        SalesMan salesManA = new SalesMan(new StrategyA());
        salesManA.salesManShow();
        System.out.println("==============");

        // 节日B
        SalesMan salesManB = new SalesMan(new StrategyB());
        salesManB.salesManShow();

        System.out.println("==============");
        // 节日C
        SalesMan salesManC = new SalesMan(new StrategyC());
        salesManC.salesManShow();
    }
}
