package com.tactics;

// 销售员 环境角色
public class SalesMan {

    // 策略对象
    Strategy strategy;

    public SalesMan(Strategy strategy) {
        this.strategy = strategy;
    }

    // 展示 促销活动
    public void salesManShow() {
        strategy.show();
    }

}
