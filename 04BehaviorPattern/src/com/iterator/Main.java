package com.iterator;

public class Main {
    public static void main(String[] args) {
        // 创建聚合对象
        StudentAggregateImpl aggregate = new StudentAggregateImpl();
        // 添加元素
        aggregate.addStudent(new Student("zs","001"));
        aggregate.addStudent(new Student("ls","002"));
        aggregate.addStudent(new Student("ww","003"));
        aggregate.addStudent(new Student("zl","004"));
        // 迭代元素
        StudentIterator iterator = aggregate.getStudentIterator();

        while (iterator.hsaNext()) {
            Student next = iterator.next();
            System.out.println(next);
        }

    }
}
