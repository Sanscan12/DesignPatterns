package com.iterator;

// 抽象聚合
public interface StudentAggregate {

    // 添加学生
    void addStudent(Student student);
    // 删除学生
    void removeStudent(Student student);
    // 获取迭代器对象
    StudentIterator getStudentIterator();

}
