package com.iterator;

import java.util.List;

// 具体迭代器
public class StudentIteratorImpl implements StudentIterator{

    // 学生集合
    private List<Student> studentList;
    // 记录指针位置
    private int position = 0;

    public StudentIteratorImpl(List<Student> studentList) {
        this.studentList = studentList;
    }

    @Override
    public boolean hsaNext() {
        return position < this.studentList.size();
    }

    @Override
    public Student next() {
        Student currentStudent = studentList.get(position);
        position++;
        return currentStudent;
    }
}
