package com.iterator;

// 抽象迭代器
public interface StudentIterator {
    // 判断下一个元素是否存在
    boolean hsaNext();
    // 获取下一个元素
    Student next();
}
