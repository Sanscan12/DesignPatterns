package com.memento.black_box;

// 备忘录管理对象
public class RoleStateCaretaker {

    // 备忘录
    private Memento memento;

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}
