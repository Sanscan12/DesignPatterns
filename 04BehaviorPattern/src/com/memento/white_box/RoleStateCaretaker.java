package com.memento.white_box;

// 备忘录管理对象 管理者
public class RoleStateCaretaker {

    // 备忘录 (恢复一次历史的状态
    private RoleStateMemento roleStateMemento;

    // 访问
    public RoleStateMemento getRoleStateMemento() {
        return roleStateMemento;
    }

    // 存储
    public void setRoleStateMemento(RoleStateMemento roleStateMemento) {
        this.roleStateMemento = roleStateMemento;
    }
}
