package com.memento.white_box;

// 备忘录模式 白盒测试
public class Main {
    public static void main(String[] args) {
        System.out.println("==========大战BOSS前==========");

        // 创建游戏对象
        GameRole gameRole = new GameRole();
        // 初始化操作
        gameRole.initState();
        System.out.println(gameRole);

        // 游戏状态备份
        RoleStateCaretaker roleStateCaretaker = new RoleStateCaretaker();
        roleStateCaretaker.setRoleStateMemento(gameRole.saveState());

        System.out.println("==========大战BOSS后==========");

        // 攻击
        gameRole.fight();
        System.out.println(gameRole);

        System.out.println("==========恢复之前的状态==========");

        gameRole.recoverState(roleStateCaretaker.getRoleStateMemento());
        System.out.println(gameRole);
    }
}
