package com.memento.white_box;

// 游戏角色 发起人角色
public class GameRole {

    // 生命/攻击/防御
    private int vit;
    private int atk;
    private int def;

    // 初始状态
    public void initState() {
        this.vit = 100;
        this.atk = 100;
        this.def = 100;
    }

    // 攻击
    public void fight() {
        this.vit = 0;
        this.atk = 0;
        this.def = 0;
    }

    // 保存状态
    public RoleStateMemento saveState() {
        return new RoleStateMemento(this.vit, this.atk, this.def);
    }

    // 恢复状态
    public void recoverState(RoleStateMemento roleStateMemento) {
        // 将备忘录对象中的状态赋值给当前对象的成员
        this.vit = roleStateMemento.getVit();
        this.atk = roleStateMemento.getAtk();
        this.def = roleStateMemento.getDef();
    }

    @Override
    public String toString() {
        return "状态{" +
                "生命力=" + vit +
                ", 攻击力=" + atk +
                ", 防御力=" + def +
                '}';
    }

    public int getVit() {
        return vit;
    }

    public void setVit(int vit) {
        this.vit = vit;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }
}
