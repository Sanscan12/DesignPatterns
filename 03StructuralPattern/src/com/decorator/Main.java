package com.decorator;

// 装饰者模式 测试类
public class Main {
    public static void main(String[] args) {
        // 点份炒饭
        FastFood food = new FriedRice();
        System.out.println(food.getDesc() + " : " + food.cost()+"元");

        // 加鸡蛋
        food = new Egg(food);
        System.out.println(food.getDesc() + " : " + food.cost()+"元");

        // 再加鸡蛋
        food = new Egg(food);
        System.out.println(food.getDesc() + " : " + food.cost()+"元");

        // 加培根
        food = new Bacon(food);
        System.out.println(food.getDesc() + " : " + food.cost()+"元");
    }
}
