package com.decorator;

// 具体构件角色 炒面类
public class FriedNoodles extends FastFood{

    public FriedNoodles() {
        super(14,"炒面");
    }

    @Override
    public float cost() {
        return getPrice();
    }


}
