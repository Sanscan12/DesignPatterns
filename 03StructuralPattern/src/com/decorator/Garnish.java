package com.decorator;

// 抽象装饰角色 装饰者类
public abstract class Garnish extends FastFood{

    // 快餐类变量
    private FastFood fastFood;

    public Garnish(FastFood fastFood, float price, String desc) {
        super(price, desc);
        this.fastFood = fastFood;
    }

    public FastFood getFastFood() {
        return fastFood;
    }
}
