package com.adapter.structural;

// 计算机
public class Computer {

    public String readSD(SDCard card) {
        if (card == null) {
            System.out.println("无法读取 SD卡");
            return null;
        }
        return card.readSD();
    }

}
