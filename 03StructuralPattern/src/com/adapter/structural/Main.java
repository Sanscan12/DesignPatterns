package com.adapter.structural;

// 适配器模式 测试类
// 电脑只能读取SD卡的信息
public class Main {
    public static void main(String[] args) {

        Computer computer = new Computer();

        // 电脑直接读取SD卡
        SDCard sdCard = new SDcardImpl();
        System.out.println(computer.readSD(sdCard));
        System.out.println("=======");
        // 电脑通过适配器读取TF卡
        SDAdapterTF adapter = new SDAdapterTF();
        System.out.println(computer.readSD(adapter));

    }
}
