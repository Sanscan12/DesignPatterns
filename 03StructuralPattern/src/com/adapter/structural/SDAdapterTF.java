package com.adapter.structural;

// 适配器类 (SD兼容TF)
// 实现当前业务接口 SDCard ; 继承了已有组件库 TFCardImpl
public class SDAdapterTF extends TFCardImpl implements SDCard{

    @Override
    public String readSD() {
        System.out.println("适配器 读TF卡");
        return readTF();
    }

    @Override
    public void writeSD(String msg) {
        System.out.println("适配器 写TF卡");
        writeTF(msg);
    }

}
