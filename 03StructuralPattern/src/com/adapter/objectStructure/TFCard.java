package com.adapter.objectStructure;

// TF卡
public interface TFCard {
    // 读
    String readTF();
    // 写
    void writeTF(String msg);
}
