package com.adapter.objectStructure;

// 适配者类 TF卡 接口实现类
public class TFCardImpl implements TFCard {
    @Override
    public String readTF() {
        return "(TF)读: hello word TF";
    }

    @Override
    public void writeTF(String msg) {
        System.out.println("(TF)写: "+msg);
    }


}
