package com.adapter.objectStructure;

// SD卡 接口
public interface SDCard {
    // 读
    String readSD();
    // 写
    void writeSD(String msg);
}
