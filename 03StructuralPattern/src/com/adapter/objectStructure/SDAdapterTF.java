package com.adapter.objectStructure;

// 适配器类 (SD兼容TF)
// 实现当前业务接口 SDCard ; 继承了已有组件库 TFCardImpl
public class SDAdapterTF implements SDCard {

    // 适配者类
    private TFCard tfCard;

    public SDAdapterTF(TFCard tfCard) {
        this.tfCard = tfCard;
    }

    @Override
    public String readSD() {
        System.out.println("适配器 读TF卡");
        return tfCard.readTF();
    }

    @Override
    public void writeSD(String msg) {
        System.out.println("适配器 写TF卡");
        tfCard.writeTF(msg);
    }

}
