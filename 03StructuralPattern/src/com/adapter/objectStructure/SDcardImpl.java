package com.adapter.objectStructure;

// 目标类 SC卡 接口实现类
public class SDcardImpl implements SDCard {
    @Override
    public String readSD() {
        return "(SD)读: hello word SD";
    }

    @Override
    public void writeSD(String msg) {
        System.out.println("(SD)写: "+msg);
    }
}
