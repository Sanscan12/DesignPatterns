package com.bridging;

// 扩展抽象化角色  Mac系统类
public class Mac extends OperatingSystem{
    public Mac(VideoFile videoFile) {
        super(videoFile);
    }

    @Override
    public void play(String fileName) {
        System.out.println("Mac系统解析: "+fileName);
        videoFile.decode(fileName);
    }
}
