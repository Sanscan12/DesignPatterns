package com.bridging;

// 扩展抽象化角色 win系统类
public class Windows extends OperatingSystem{

    public Windows(VideoFile videoFile) {
        super(videoFile);
    }

    @Override
    public void play(String fileName) {
        System.out.println("Windows系统解析: "+fileName);
        videoFile.decode(fileName);
    }
}
