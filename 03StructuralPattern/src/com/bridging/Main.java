package com.bridging;

// 桥接模式 测试类
public class Main {
    public static void main(String[] args) {

        OperatingSystem os = new Windows(new AVIFile());
        os.play("战狼3");

        System.out.println("==========");

        OperatingSystem os2 = new Mac(new RmvbFIle());
        os2.play("让子弹飞");

    }
}
/** 结果
 *  Windows系统解析: 战狼3
 *  (解码)avi视频文件:战狼3
 *  ==========
 *  Mac系统解析: 让子弹飞
 *  (解码)rmvb文件: 让子弹飞
 */
