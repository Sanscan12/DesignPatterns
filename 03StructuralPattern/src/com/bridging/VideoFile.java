package com.bridging;

// 实现化角色 视频文件类
public interface VideoFile {
    // 解码功能
    void decode(String fileName);
}
