package com.bridging;

// 抽象化角色 操作系统类
public abstract class OperatingSystem{

    protected VideoFile videoFile;

    public OperatingSystem(VideoFile videoFile) {
        this.videoFile = videoFile;
    }

    public abstract void play(String fileName);
}
