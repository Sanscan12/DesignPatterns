package com.exterior;

// 外观角色 小爱同学
public class IntelligentControlLittleLove {

    private AirCondition airCondition;
    private Light light;
    private Tv tv;

    public IntelligentControlLittleLove() {
        airCondition = new AirCondition();
        light = new Light();
        tv = new Tv();
    }

    public void say(String msg) {
        // 处理语言(关键字)...
        // 检索关键字信息
        // 17jdk 特性写法
        switch (msg) {
            case "打开" -> on();
            case "关闭" -> off();
            // ...
            default -> System.out.println("未能理解!");
        }
    }

    public void on() {
        airCondition.on();
        light.on();
        tv.on();
        System.out.println("打开所有家电");
    }
    public void off() {
        airCondition.off();
        light.off();
        tv.off();
        System.out.println("关闭所有家电");
    }

}
