package com.exterior;

// 外观模式 测试类
public class Main {
    public static void main(String[] args) {

        IntelligentControlLittleLove littleLove = new IntelligentControlLittleLove();

        littleLove.say("打开");
        System.out.println("==========");
        littleLove.say("关闭");

    }
}
