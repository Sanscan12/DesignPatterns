package com.exterior;

// 子系统角色 灯光类
public class Light {

    public void on() {
        System.out.println("开灯了");
    }

    public void off() {
        System.out.println("关灯了");
    }

}
