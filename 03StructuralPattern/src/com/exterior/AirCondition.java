package com.exterior;

// 子系统角色 空调类
public class AirCondition {

    public void on() {
        System.out.println("开空调了");
    }

    public void off() {
        System.out.println("关空调了");
    }

}
