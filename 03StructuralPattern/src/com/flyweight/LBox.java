package com.flyweight;

// 具体享元角色 L盒子类
public class LBox extends AbstractBox{
    @Override
    public String getShape() {
        return "L";
    }
}
