package com.flyweight;

// 享元模式 测试类
public class Main {
    public static void main(String[] args) {

        // 获取I图形对象
        AbstractBox iBox = BoxFactory.getInstance().getShape("I");
        iBox.display("red");

        AbstractBox LBox = BoxFactory.getInstance().getShape("L");
        LBox.display("green");

        AbstractBox oBox = BoxFactory.getInstance().getShape("O");
        oBox.display("gray");
        AbstractBox oBox2 = BoxFactory.getInstance().getShape("O");
        oBox2.display("pink");

        System.out.println("oBox == oBox2?" + (oBox == oBox2));

    }
}
