package com.flyweight;

// 抽象享元角色 抽象盒类
public abstract class AbstractBox {
    // 形状获取
    public abstract String getShape();

    public void display(String color) {
        System.out.println("方块形状: "+this.getShape()+" 颜色: "+color);
    }
}
