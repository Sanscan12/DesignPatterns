package com.flyweight;

// 具体享元角色 I盒子类
public class IBox extends AbstractBox{
    @Override
    public String getShape() {
        return "I";
    }
}
