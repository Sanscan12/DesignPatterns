package com.flyweight;

// 具体享元角色 O盒子类
public class OBox extends AbstractBox{
    @Override
    public String getShape() {
        return "O";
    }
}
