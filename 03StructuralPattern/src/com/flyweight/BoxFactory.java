package com.flyweight;

import java.util.HashMap;

// 享元工厂角色 盒子工厂类
public class BoxFactory {

    private static HashMap<String, AbstractBox> map;

    private BoxFactory() {
        map = new HashMap<>();
        AbstractBox iBox = new IBox();
        AbstractBox lBox = new LBox();
        AbstractBox oBox = new OBox();
        map.put("I",iBox);
        map.put("L",lBox);
        map.put("O",oBox);
    }

    private static class SingletonHolder {
        private static final BoxFactory INSTANCE = new BoxFactory();
    }

    public static BoxFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public AbstractBox getShape(String key) {
        return map.get(key);
    }

}
