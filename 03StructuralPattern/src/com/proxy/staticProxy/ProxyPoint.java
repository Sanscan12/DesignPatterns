package com.proxy.staticProxy;

// 代理点
public class ProxyPoint implements SellTickets{

    // 火车站对象
    private TrainStation station = new TrainStation();

    @Override
    public void sell() {
        System.out.println("代理点 卖票(额付中介费)");
        station.sell();
    }
}
