package com.proxy.staticProxy;

// 卖票 接口
public interface SellTickets {
    void sell();
}
