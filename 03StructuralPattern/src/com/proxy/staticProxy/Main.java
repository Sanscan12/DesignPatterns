package com.proxy.staticProxy;

// 代理模式 静态代理模式 测试类
public class Main {
    public static void main(String[] args) {

        ProxyPoint pp = new ProxyPoint();
        pp.sell();

    }
}
/** 结果
 * 代理点 卖票(额付中介费)
 * 火车站 卖票
 */