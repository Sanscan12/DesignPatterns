package com.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

// 代理对象工厂
public class ProxyFactory {

    // 火车站对象
    private TrainStation station = new TrainStation();

    public TrainStation getProxyObject() {
        // 创建 Enhancer对象 (类似jdk中的Proxy类获取代理的方式)
        /** 代理对象为子类 , 目标对象为父类
         * 参数:
         *   1. 父类为字节码对象
         *   2. 回调函数
         */
        return (TrainStation) Enhancer.create(TrainStation.class,
                (MethodInterceptor) (o, method, objects, methodProxy) -> {
                    // 业务增强
                    System.out.println("卖票 前置通知");
                    Object invoke = method.invoke(station, objects);
                    System.out.println("卖票 后置通知");
                    return invoke;
                });
    }
}
