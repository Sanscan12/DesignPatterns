package com.proxy.cglib;

// 火车站
// cglib代理 无需实现接口
public class TrainStation {
    public void sell() {
        System.out.println( "火车站 卖票");
    }
}
