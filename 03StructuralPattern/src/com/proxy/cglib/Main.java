package com.proxy.cglib;

// 代理模式 cglib动态代理 测试类
public class Main {
    public static void main(String[] args) {

        ProxyFactory factory = new ProxyFactory();
        TrainStation proxyObject = factory.getProxyObject();
        // 卖票
        proxyObject.sell();

    }
}
