package com.proxy.jdk;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

// 代理工厂
// PS: 该工厂不是代理类 , 而是程序运行过程在内存生成的类
public class ProxyFactory {

    // 目标对象
    private TrainStation station = new TrainStation();

    // 返回单例对象
    public SellTickets getProxyObject() {
        // newProxyInstance()方法
        //  参数 : ClassLoader类加载器 & Class代理类实现的接口字节码对象 & InvocationHandler代理对象的处理程序
        return (SellTickets) Proxy.newProxyInstance(
                station.getClass().getClassLoader(),
                station.getClass().getInterfaces(),
                /** invoke()方法
                 * 参数说明 :
                 *      Object proxy : 代理对象
                 *      Method method : 对接口的方法进行封装成的对象
                 *      Object[] args : 调用方法的参数
                 *  返回 : 对应方法执行的返回值 (如果 void 则 null)
                 */
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        // 增强业务
                        System.out.println("卖票 前置通知");
                        // 执行 业务方法 , 并且返回
                        Object obj = method.invoke(station, args);
                        System.out.println("卖票 后置通知");
                        return obj;
                    }
                }
        );
    }

}
