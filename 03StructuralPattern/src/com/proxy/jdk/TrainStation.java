package com.proxy.jdk;

// 火车站
public class TrainStation implements SellTickets {
    @Override
    public void sell() {
        System.out.println( "火车站 卖票");
    }
}
