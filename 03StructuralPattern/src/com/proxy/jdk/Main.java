package com.proxy.jdk;

// 代理模式 jdk动态代理 测试类
public class Main {
    public static void main(String[] args) {

        // 获取代理工厂代理对象
        ProxyFactory factory = new ProxyFactory();
        // 获取代理对象 (该对象是动态生成的
        SellTickets proxyObject = factory.getProxyObject();
        // 卖票方法
        proxyObject.sell();

        System.out.println("proxyObject = " + proxyObject.getClass());

        while (true) {}
    }
}
/** 运行结果
 *   卖票 前置通知
 *   火车站 卖票
 *   卖票 后置通知
 *   proxyObject = class com.sun.proxy.$Proxy0
 */
