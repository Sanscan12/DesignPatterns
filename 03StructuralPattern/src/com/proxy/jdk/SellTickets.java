package com.proxy.jdk;

// 卖票 接口
public interface SellTickets {
    void sell();
}
