package com.combination;

// 抽象根节点 菜单组件类 (菜单/菜单项 两种可能)
public abstract class MenuComponent {
    // 菜单组件名称&菜单组件层级
    protected String name;
    protected int level;

    // 添加菜单
    public void add(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }

    // 移出菜单
    public void remove(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }

    // 获取指定的子菜单
    public MenuComponent getChild(int index) {
        throw new UnsupportedOperationException();
    }

    // 打印菜单名称和方法
    public abstract void print();

}
