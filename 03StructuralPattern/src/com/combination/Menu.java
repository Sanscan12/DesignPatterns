package com.combination;

import java.util.ArrayList;
import java.util.List;

// 树枝节点 菜单类
public class Menu extends MenuComponent {

    // 菜单可以有多个 子菜单/子菜单项
    private List<MenuComponent> menuComponentList = new ArrayList<>();

    public Menu(String name, int level) {
        this.level = level;
        this.name = name;
    }

    @Override
    public void add(MenuComponent menuComponent) {
        menuComponentList.add(menuComponent);
    }

    @Override
    public void remove(MenuComponent menuComponent) {
        menuComponentList.remove(menuComponent);
    }

    @Override
    public MenuComponent getChild(int index) {
        return menuComponentList.get(index);
    }

    @Override
    public void print() {
        for (int i = 0; i < level - 1; i++) {
            System.out.print("--");
        }
        // 当前菜单
        System.out.println(name);
        // 子菜单/子菜单项
        for (MenuComponent menuComponent : menuComponentList) {
            menuComponent.print();
        }
    }


}
