package com.combination;

// 叶子节点 菜单项类
public class MenuItem extends MenuComponent {

    public MenuItem(String name, int level) {
        this.name = name;
        this.level = level;
    }

    @Override
    public void print() {
        for (int i = 0; i < level - 1; i++) {
            System.out.print("--");
        }
        System.out.println(name);
    }
}
