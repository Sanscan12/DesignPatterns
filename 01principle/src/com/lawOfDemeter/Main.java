package com.lawOfDemeter;

/*
* 测试
*
* */
public class Main {
    public static void main(String[] args) {

        Agent agent = new Agent();
        Star star = new Star("Sans");
        agent.setStar(star);

        Fans fans = new Fans("张三");
        agent.setFans(fans);

        Company company = new Company("柏竹科技");
        agent.setCompany(company);

        // 粉丝见面
        agent.meeting();
        // 公司合作
        agent.bushiness();

    }
}
