package com.lawOfDemeter;

// 经纪人类
public class Agent {

    private Star star;
    private Fans fans;
    private Company company;

    // 粉丝见面
    public void meeting() {
        System.out.println(fans.getName()+" 粉丝与 "+star.getName()+" 见面了!");
    }

    // 和媒体公司谈业务
    public void bushiness() {
        System.out.println(star.getName()+" 明星与 "+company.getName()+" 谈义务!");
    }

    public void setStar(Star star) {
        this.star = star;
    }

    public void setFans(Fans fans) {
        this.fans = fans;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
