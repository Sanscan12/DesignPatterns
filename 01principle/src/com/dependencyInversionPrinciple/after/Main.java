package com.dependencyInversionPrinciple.after;

/**
 * 测试类
 *  通过接口化形式 , 可以任意更改品牌 , 只需接口实现即可
 */
public class Main {
    public static void main(String[] args) {
        // 创建组件对象
        Cpu cpu = new IntelCpu();
        Ram ram = new HSDRam();
        HardDisk hardDisk = new DWHardDisk();


        // 组装主机
        Computer computer = new Computer();
        computer.setCpu(cpu);
        computer.setRam(ram);
        computer.setHardDisk(hardDisk);

        // 运行主机
        computer.run();
    }
}
