package com.dependencyInversionPrinciple.after;

// 西数硬盘
public class DWHardDisk implements HardDisk{

    private String data;

    // 存储数据方法
    public void save(String data) {
        System.out.println("西数硬盘存储数据 : "+data);
        this.data = data;
    }

    // 获取数据
    public String get() {
        System.out.println("获取西数硬盘存储数据!");
        return data;
    }

}
