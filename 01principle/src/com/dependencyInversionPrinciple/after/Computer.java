package com.dependencyInversionPrinciple.after;

// 计算机
public class Computer {

    // 硬盘 & cpu & 内存
    private HardDisk hardDisk;
    private Cpu cpu;
    private Ram ram;

    public void run() {
        System.out.println("主机运行!");
        String data = hardDisk.get();
        System.out.println("硬盘读取 !"+data);
        cpu.run();
        ram.save();
    }

    public HardDisk getHardDisk() {
        return hardDisk;
    }

    public void setHardDisk(HardDisk hardDisk) {
        this.hardDisk = hardDisk;
    }

    public Cpu getCpu() {
        return cpu;
    }

    public void setCpu(Cpu cpu) {
        this.cpu = cpu;
    }

    public Ram getRam() {
        return ram;
    }

    public void setRam(Ram ram) {
        this.ram = ram;
    }
}
