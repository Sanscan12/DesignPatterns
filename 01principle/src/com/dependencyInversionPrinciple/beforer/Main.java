package com.dependencyInversionPrinciple.beforer;

/**
 * 测试类 (反例测试
 *  装配的电脑只能使用固定的品牌 , 没有自定义的一个应用
 */
public class Main {
    public static void main(String[] args) {
        // 创建组件对象
        DWHardDisk DWHardDisk = new DWHardDisk();
        IntelCpu intelCpu = new IntelCpu();
        HSDRam HSDRam = new HSDRam();

        // 组装主机
        Computer computer = new Computer();
        computer.setIntelCpu(intelCpu);
        computer.setHsdRam(HSDRam);
        computer.setDwHardDisk(DWHardDisk);

        // 运行主机
        computer.run();
    }
}
