package com.dependencyInversionPrinciple.beforer;

// 计算机
public class Computer {

    // 硬盘 & cpu & 内存
    private DWHardDisk dwHardDisk;
    private IntelCpu intelCpu;
    private HSDRam hsdRam;

    public void run() {
        System.out.println("主机运行!");
        String data = dwHardDisk.get();
        System.out.println("硬盘读取 !"+data);
        intelCpu.run();
        hsdRam.save();
    }

    public DWHardDisk getDwHardDisk() {
        return dwHardDisk;
    }

    public void setDwHardDisk(DWHardDisk dwHardDisk) {
        this.dwHardDisk = dwHardDisk;
    }

    public IntelCpu getIntelCpu() {
        return intelCpu;
    }

    public void setIntelCpu(IntelCpu intelCpu) {
        this.intelCpu = intelCpu;
    }

    public HSDRam getHsdRam() {
        return hsdRam;
    }

    public void setHsdRam(HSDRam hsdRam) {
        this.hsdRam = hsdRam;
    }
}
