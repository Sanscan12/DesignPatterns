package com.openclosedprinciple;

// 客户 (用户)
public class Client {

    public static void main(String[] args) {
        // 1. 创建 系统主题对象
        SysTheme sysTheme = new SysTheme();
        // 2. 创建 默认主题
        DefaultSkin skin = new DefaultSkin();
        // 3. 将系统 设置默认主题
        sysTheme.setSkin(skin);
        // 3.1 修改为 我的皮肤
        sysTheme.setSkin(new MySkin());

        // 4. 显示皮肤
        sysTheme.display();

    }

}
