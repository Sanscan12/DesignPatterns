package com.openclosedprinciple;

// 系统主题
public class SysTheme {

    // 皮肤类 (抽象)
    private AbstractSkin skin;

    public void setSkin(AbstractSkin skin) {
        this.skin = skin;
    }

    // 展示皮肤
    public void display() {
        skin.display();
    }

}
