package com.interfaceSegregationPrinciple.after;

public class TCLSafetyDoor implements AntiTheft , Waterproof{
    @Override
    public void antiTheft() {
        System.out.println("TOC安全门 防盗");
    }

    @Override
    public void waterproof() {
        System.out.println("TOC安全门 防水");
    }
}
