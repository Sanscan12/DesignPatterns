package com.interfaceSegregationPrinciple.after;

// 防水
public interface Waterproof {
    void waterproof();
}
