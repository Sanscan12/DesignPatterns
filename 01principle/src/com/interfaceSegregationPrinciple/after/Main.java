package com.interfaceSegregationPrinciple.after;

/* 测试 (依赖倒转
*   将防盗门的功能以单元形式分开 , 灵活性更高
* */
public class Main {
    public static void main(String[] args) {
        TCLSafetyDoor door = new TCLSafetyDoor();
        // 很显然可以看到只有 防水&防盗
        door.waterproof();
        door.antiTheft();
    }
}
