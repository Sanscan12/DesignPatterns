package com.interfaceSegregationPrinciple.before;

/*
* 测试 反例
*  假如该 TCL品牌 不支持 防火功能
* */
public class Main {

    public static void main(String[] args) {
        TCLSafetyDoor safetyDoor = new TCLSafetyDoor();
        safetyDoor.antiTheft();
        safetyDoor.fireproof();
        safetyDoor.waterproof();
    }

}
