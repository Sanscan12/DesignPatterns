package com.interfaceSegregationPrinciple.before;

// TCL品牌安全门
public class TCLSafetyDoor implements SafetyDoor {

    @Override
    public void antiTheft() {
        System.out.println("TOC安全门 防盗");
    }

    @Override
    public void fireproof() {
        System.out.println("TOC安全门 防火");
    }

    @Override
    public void waterproof() {
        System.out.println("TOC安全门 防水");
    }
}
