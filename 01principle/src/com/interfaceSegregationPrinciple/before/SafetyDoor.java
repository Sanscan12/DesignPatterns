package com.interfaceSegregationPrinciple.before;

// 安全门
public interface SafetyDoor {

    // 防盗
    void antiTheft();
    // 防火
    void fireproof();
    // 防水
    void waterproof();

}
