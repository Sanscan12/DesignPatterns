package com.syntheticReusePrinciple.polymerization;

// 汽油车
public class PetrolCar extends Car {
    public void move() {
        System.out.println("汽油车 移动");
    }
}
