package com.syntheticReusePrinciple.inherit;

// 电力汽车
public class ElectricCar extends Car{
    public void move() {
        System.out.println("电力汽车 移动");
    }
}
