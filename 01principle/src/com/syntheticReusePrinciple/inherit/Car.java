package com.syntheticReusePrinciple.inherit;

// 汽车类
public class Car {

    // 移动类
    public void move() {
        System.out.println("汽车移动");
    }
}
