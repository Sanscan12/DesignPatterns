package com.syntheticReusePrinciple.inherit;

public class RedPetrolCar extends  PetrolCar{
    public void move() {
        System.out.println("红色 汽油车 移动");
    }
}
