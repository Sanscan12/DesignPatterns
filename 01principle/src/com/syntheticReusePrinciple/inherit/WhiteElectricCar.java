package com.syntheticReusePrinciple.inherit;

public class WhiteElectricCar extends ElectricCar{
    public void move() {
        System.out.println("白色 电力汽车 移动");
    }
}
