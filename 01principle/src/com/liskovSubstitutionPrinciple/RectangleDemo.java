package com.liskovSubstitutionPrinciple;

// main
public class RectangleDemo{


    public static void resize(Rectangle rectangle) {
        while (rectangle.getWidth() <= rectangle.getLength()) {
            rectangle.setWidth(rectangle.getWidth() + 1);
        }
    }

    public static void printLengthAndWidth(Quadrilateral rectangle) {
        System.out.println("Length : " + rectangle.getLength());
        System.out.println("Width : " + rectangle.getWidth());
    }

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.setLength(20);
        rectangle.setWidth(10);
        printLengthAndWidth(rectangle);
        /*  Length : 20.0
            Width : 10.0
        * */
        resize(rectangle);
        printLengthAndWidth(rectangle);
        /*  Length : 20.0
            Width : 21.0
        * */
        System.out.println("============");

        Square square = new Square();
        square.setSide(10);
        // 报红 , 很显然 这个对象不匹配长方形对象
//        resize(square);
        printLengthAndWidth(square);

//        Rectangle rectangle1 = new Square();
//        rectangle1.setLength(10);
//        可以发现 正方形怎样调整宽高都是一致的变化
//        这里很显然 重写了父类的 setLength() 和 setWidth() 方法 , 违背了 里氏代换原则
//        resize(rectangle1);
//        printLengthAndWidth(rectangle1);

    }

}
