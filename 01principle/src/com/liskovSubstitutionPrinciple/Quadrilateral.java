package com.liskovSubstitutionPrinciple;

// 四边形接口
public interface Quadrilateral {

    double getLength();
    double getWidth();

}
