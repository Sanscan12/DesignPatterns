package com.liskovSubstitutionPrinciple;

// 正方形
public class Square implements Quadrilateral {

    private double side;


    @Override
    public double getLength() {
        return side;
    }

    @Override
    public double getWidth() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}
