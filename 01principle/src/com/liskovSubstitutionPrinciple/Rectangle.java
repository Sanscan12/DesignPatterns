package com.liskovSubstitutionPrinciple;

// 长方形
public class Rectangle implements Quadrilateral {

    double length;
    double width;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
