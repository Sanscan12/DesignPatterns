package com.sans;

import com.framework.context.ApplicationContext;
import com.framework.context.support.ClassPathXmlApplicationContext;
import com.sans.service.inpl.UserServiceImpl;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;

// Spring 执行测试
public class Main {

    public static void main(String[] args) {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");

        // 延迟加载
//        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("application.xml"));

        UserServiceImpl userService = null;
        try {
            userService = applicationContext.getBean("userService", UserServiceImpl.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        userService.add();

    }

}
