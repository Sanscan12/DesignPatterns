package com.sans.dao.impl;

import com.sans.dao.UserDao;

public class UserDaoImpl implements UserDao {

    private String username;
    private String password;

    public UserDaoImpl() {
        System.out.println("UserDaoImpl()构造方法...");
    }

    @Override
    public void add() {
        System.out.println("UserDao.add()...");
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "UserDaoImpl{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
