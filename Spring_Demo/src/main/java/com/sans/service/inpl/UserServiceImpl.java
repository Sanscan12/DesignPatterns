package com.sans.service.inpl;

import com.sans.dao.UserDao;
import com.sans.dao.impl.UserDaoImpl;
import com.sans.service.UserService;

public class UserServiceImpl implements UserService {

    private UserDao userdao;

    public UserServiceImpl() {
        System.out.println("UserService()构造方法...");
    }

    // set注入
    public void setUserdao(UserDao userdao) {
        this.userdao = userdao;
    }

    @Override
    public void add() {
        System.out.println("UserService.add()...");
        userdao.add();
    }
}
