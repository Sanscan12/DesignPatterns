package com.framework.utils;

import java.util.Locale;

// 字符串工具类
public class SpringUtils {

    // userDao ==> setUserDao
    public static String getSetterMethodFieldName(String filedName) {
        return "set" + filedName.substring(0, 1).toUpperCase(Locale.ROOT) + filedName.substring(1);
    }

}
