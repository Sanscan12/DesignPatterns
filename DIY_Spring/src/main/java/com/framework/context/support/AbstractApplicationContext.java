package com.framework.context.support;

import com.framework.beans.BeanDefinition;
import com.framework.beans.factory.support.BeanDefinitionReader;
import com.framework.beans.factory.support.BeanDefinitionRegistry;
import com.framework.context.ApplicationContext;

import java.util.HashMap;
import java.util.Map;

// applicationContext 非延迟加载 , 并存储Bean对象的容器
public abstract class AbstractApplicationContext implements ApplicationContext {

    // 解析器变量
    protected BeanDefinitionReader beanDefinitionReader;
    // 存储Bean对象
    protected Map<String,Object> singletonObjects = new HashMap<>();
    // 定义资源文件路径
    protected String configLocation;

    @Override
    public void refresh() throws  Exception {
        // 加载资源文件
        beanDefinitionReader.loadBeanDefinitions(configLocation);
        // 初始化Bean
        finishBeanInitialization();
    }

    // 初始化bean
    private void finishBeanInitialization() throws Exception {
        // 获取 注册表
        BeanDefinitionRegistry registry = beanDefinitionReader.getRegistry();
        // 获取 集合对象名称
        String[] beanNames = registry.getBeanDefinitionNames();
        for (String beanName : beanNames) {
            // Bean 对象
            BeanDefinition beanDefinition = registry.getBeanDefinition(beanName);
            // 进行初始化Bean
            getBean(beanName);
//            singletonObjects.put(beanName , beanDefinition);
        }
    }
}
