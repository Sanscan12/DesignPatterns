package com.framework.context;

import com.framework.beans.factory.BeanFactory;

// 非延迟加载功能
public interface ApplicationContext extends BeanFactory {

    // 进行配置文件加载并创建对象
    void refresh() throws Exception;

}
