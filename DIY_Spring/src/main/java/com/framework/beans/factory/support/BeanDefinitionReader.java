package com.framework.beans.factory.support;

import com.framework.beans.factory.support.BeanDefinitionRegistry;

// 解析配置文件 , 并在注册表中注册bean信息
public interface BeanDefinitionReader {

    // 获取 注册表对象
    BeanDefinitionRegistry getRegistry();
    // 加载 配置文件并进行注册
    void loadBeanDefinitions(String configLocation) throws Exception;

}
