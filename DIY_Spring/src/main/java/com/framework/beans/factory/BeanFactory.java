package com.framework.beans.factory;

// Ioc 容器父接口
public interface BeanFactory {

    // 获取 按名称获取bean对象
    Object getBean(String name) throws  Exception;

    // 获取 按名称获取bean对象 , 并进行类型转换
    <T> T getBean(String name , Class<? extends T> clazz) throws Exception;

}
