package com.framework.beans.factory.support;

import com.framework.beans.BeanDefinition;

// 注册表接口
public interface BeanDefinitionRegistry {

    // 添加 注册 BeanDefinitionRegistry对象 到注册表中
    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition);
    // 删除 根据指定名称删除 BeanDefinitionRegistry对象 (从注册表中
    void removeBeanDefinition(String beanName) throws Exception;
    // 获取 指定名称的 BeanDefinitionRegistry对象 (从注册表中
    BeanDefinition getBeanDefinition(String beanName);
    // 包含 指定名称的 BeanDefinitionRegistry对象 (从注册表中
    boolean containsBeanDefinition(String beanName);
    // 获取 注册表中所有对象 个数
    int getBeanDefinitionCount();
    // 获取 注册表中所有对象 名称
    String[] getBeanDefinitionNames();

}
